import 'package:flutter/material.dart';
enum APP_THEME{LIGHT,DARK}
void main() {
  runApp(ContactProfilePage());
}
class MyAppTheme {
  static ThemeData appThemelight(){
    return ThemeData(
      brightness: Brightness.light,
      appBarTheme: AppBarTheme(
        color: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
      ),
      iconTheme: IconThemeData(color: Colors.greenAccent,
      ),
    );
  }
  static ThemeData appThemeDark(){
    return ThemeData(
      brightness: Brightness.dark,
      appBarTheme: AppBarTheme(
        color: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
      ),
      iconTheme: IconThemeData(color: Colors.greenAccent,
      ),
    );
  }
}
class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}
class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme== APP_THEME.DARK
          ? MyAppTheme.appThemelight()
          :MyAppTheme.appThemeDark(),
      home: Scaffold(
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.threesixty),
          onPressed: () {
            setState(() {
              currentTheme== APP_THEME.DARK
                  ? currentTheme = APP_THEME.LIGHT
                  : currentTheme = APP_THEME.DARK;
            },
            );
          },
        ),
      ),
    );
  }


  Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}

Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.textsms,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}

Widget buildVidoCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}

Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}

Widget  buildDirectionButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Direction"),
    ],
  );
}

Widget  buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money_outlined,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}

Widget mobilePhoneListTile() {
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("330-803-3390"),
    subtitle: Text("mobile"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      // color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}

Widget otherPhoneListTile() {
  return ListTile(
    leading:  Icon(null),
    title: Text("330-803-3390"),
    subtitle: Text("mobile"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      // color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}

Widget emailListTile() {
  return ListTile(
    leading: Icon(Icons.email),
    title: Text("Gudetama@hotmail.com"),
    subtitle: Text("work"),
    trailing: IconButton(
      icon: Icon(null),
      // color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}

Widget addressListTile() {
  return ListTile(
    leading: Icon(Icons.location_on),
    title: Text("123 Bangsaen chonburi"),
    subtitle: Text("home"),
    trailing: IconButton(
      icon: Icon( Icons.directions),
      // color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}

AppBar buildAppBarWidget() {
  return AppBar(
    backgroundColor: Colors.blue.shade900,
    leading: Icon(
      Icons.arrow_back,
      color: Colors.black,
    ),
    actions: <Widget>[
      IconButton(
          icon: Icon(Icons.star_border),
          color: Colors.black,
          onPressed: () {
            print("contact is starred");
          }
      ),
    ],
  );
}

Widget buildBodyWidget() {
  return ListView(
      children: <Widget>[

        Column(
          children: <Widget>[
            Container(
              width: double.infinity,
              //Height constraint at Container widget level
              height: 200,
              child: Image.network(
                "https://www.central.co.th/e-shopping/storage/2017/12/001-dailydot.com_.png",
                fit: BoxFit.cover,

              ),
            ),
            Container(
              height: 60,
              child: Row(
                mainAxisAlignment : MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child : Text(
                      "Gudetama",
                      style: TextStyle(fontSize: 24),
                    ),
                  ),
                ],
              ),
            ),


            Container(
              margin: const EdgeInsets.only(top: 8, bottom: 8),
              child: profileActionItem(),
            ),
          ],
        ) ,
        Divider(
          color: Colors.grey,
        ),
        mobilePhoneListTile(),
        otherPhoneListTile(),

        Divider(
          color: Colors.grey,
        ),

        emailListTile(),
        Divider(
          color: Colors.grey,
        ),

        addressListTile(),
        Divider(
          color: Colors.grey,
        ),
      ]
  );

}

Widget profileActionItem(){
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildVidoCallButton(),
      buildEmailButton(),
      buildDirectionButton(),
      buildPayButton(),
    ],
  );
}
}